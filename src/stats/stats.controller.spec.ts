import { Test, TestingModule } from '@nestjs/testing';
import { StatsController } from './stats.controller';

describe('AppController', () => {
  let appController: StatsController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [StatsController],
      providers: [],
    }).compile();

    appController = app.get<StatsController>(StatsController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(appController.getStats()).toBe('Hello World!');
    });
  });
});
