import { Controller, Get, InternalServerErrorException } from '@nestjs/common';
import myCache = require('src/helper/cache');
import { readdir } from 'fs';
import { rootPath } from 'config';
import { join } from 'path';
import { promisify } from 'util';

@Controller('stats')
export class StatsController {
  @Get()
  async getStats(): Promise<any> {

    try {
      const files: string[] = await promisify(readdir)(join(rootPath, '..', 'images'));
      return {...myCache.getStats(), files: files.length};
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }
}
