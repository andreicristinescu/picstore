import { Module } from '@nestjs/common';
import { ImagesModule } from './images/images.module';
import { StatsModule } from './stats/stats.module';

@Module({
  imports: [ImagesModule, StatsModule],
})

export class AppModule {}
