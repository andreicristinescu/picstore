
import { Controller, Get, Param, Query, Res, NotFoundException } from '@nestjs/common';
import { join, basename } from 'path';
import sharp = require('sharp');

import { ImagesService } from './images.service';
import { GetOneParamsDto } from './dto/images.get.params.dto';
import { GetOneQueryDto } from './dto/images.get.query.dto';
import { rootPath } from '../../config';
import { createWriteStream, ReadStream, createReadStream, existsSync } from 'fs';
import myCache = require('src/helper/cache');

@Controller('images')
export class ImagesController {
  constructor(private readonly imageService: ImagesService) { }

  @Get(':name')
  async getImage(@Param() params: GetOneParamsDto, @Query() query: GetOneQueryDto, @Res() res) {
    let width: number;
    let height: number;
    const path = join(rootPath, '..', 'images', params.name);
    const filename = (query.size || '') + basename(path);
    const tempPath = join(rootPath, '..', 'temp', filename);

    if (myCache.get(filename)) {
      return createReadStream(tempPath).pipe(res);
    }

    if (!existsSync(path)) {
      throw new NotFoundException('No such image');
    }

    if (query.size) {
      [width, height] = query.size && query.size.split('x').map((value: string): number => +value);

      const writeStream = createWriteStream(tempPath);
      const imageResizeStream: ReadStream | sharp.Sharp = this.imageService.resize(path, width, height);

      imageResizeStream.pipe(writeStream);

      myCache.set(filename, true, 10000);

      return imageResizeStream.pipe(res);
    }

    return createReadStream(path).pipe(res);
  }
}
