import { Injectable, NotFoundException } from '@nestjs/common';
import { createReadStream, ReadStream, createWriteStream } from 'fs';
import sharp = require('sharp');

@Injectable()
export class ImagesService {
  resize(imgPath: string, width: number, height: number): sharp.Sharp {
    const readStream: ReadStream = createReadStream(imgPath);

    if (width || height) {
      const transform = sharp().resize(width, height);

      return readStream.pipe(transform);
    }

    return null;

  }
}
