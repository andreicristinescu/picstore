import { Matches } from 'class-validator';

export class GetOneParamsDto {
  @Matches(/^[\w-]+\.(png|jpg|gif|bmp|jpeg|PNG|JPG|GIF|BMP)$/, 'g')
  readonly name: string;
}
