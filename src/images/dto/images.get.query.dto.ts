import { Matches, IsOptional } from 'class-validator';

export class GetOneQueryDto {
  @IsOptional()
  @Matches(/^\d+x\d+$/, 'i')
  readonly size: string;
}
