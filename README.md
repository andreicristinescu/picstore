# picstore

## Description
This service can be used to serve images optimised for the device they�ll be displayed onto
## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
# API

## Get Image
Can be used to retrive an image. The client can choose the image size.

url: ```/images/:imagename.extension?size=30x30```
```
curl --request GET \
  --url 'http://localhost:3000/images/mueller.jpg?size=30x30' \
  --header 'cache-control: no-cache'
```

## Get Stats
Can be used to retrive stats about the service

url: ```/stats```

```
curl --request GET \
  --url http://localhost:3000/stats \
  --header 'cache-control: no-cache'
```

# TODO
## Add picture to cache at startup
## Remove picture after ttl
## Add unit && functional tests
